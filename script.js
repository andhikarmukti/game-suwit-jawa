function getPilihanKomputer() {
    const comp = Math.random();
    if(comp < 0.34) return 'gajah';
    if(comp >= 0.34 && comp <= 0.67) return 'orang';
    return 'semut';
};

function getHasil(comp, player) {
    if(player == comp) return 'SERI!';
    if(player == 'gajah') return (comp == 'orang') ? 'MENANG!' : 'KALAH!';
    if(player == 'orang') return (comp == 'semut') ? 'MENANG!' : 'KALAH!';
    if(player == 'semut') return (comp == 'gajah') ? 'MENANG!' : 'KALAH!';
}

function putar() {
    const gambarPutar = document.querySelector('.img-komputer');
    const gambar = ['gajah', 'orang', 'semut'];
    let i = 0;
    const waktuSaatIni = new Date().getTime();
    setInterval(function(){
        if(new Date().getTime() - waktuSaatIni > 1000){
            clearInterval;
            return;
        }
        gambarPutar.setAttribute('src', 'img/' + gambar[i++] + '.png');
        if(i == gambar.length) i = 0;
    }, 100);
}

const pilihan = document.querySelectorAll('.area-player ul li img');
pilihan.forEach(function(pil){
    pil.addEventListener('click', function(){
        const pilPlayer = pil.className;
        const pilComp = getPilihanKomputer();
        const hasil   = getHasil(pilComp, pilPlayer);

        putar();
        setTimeout(() => {
            document.querySelector('.area-komputer img').setAttribute('src', 'img/' + pilComp + '.png');
            return document.querySelector('.info').innerHTML = hasil;
        }, 1000);
    })
});